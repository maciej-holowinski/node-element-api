const boxWrapper = document.querySelector(".box-wrapper");
const existingDiv = document.querySelector(".existingDiv");

//prepere new div
const newDiv = document.createElement("div");
newDiv.textContent = "JS div";
newDiv.className = "newDiv";

//insert new div
boxWrapper.insertBefore(newDiv, existingDiv);

//SCROLL LISTENER

let offset = boxWrapper.offsetTop;
window.addEventListener("scroll", () => {
  if (window.pageYOffset > offset) {
    boxWrapper.classList.add("box-wrapper-fixed");
  } else {
    boxWrapper.classList.remove("box-wrapper-fixed");
  }
});

////////

//EVENT TARGET ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const radio1 = document.getElementById("1");
const radio2 = document.getElementById("2");
const radio3 = document.getElementById("3");

//add listener

radio1.addEventListener("change", () => {
  existingDiv.addEventListener("click", click);
  newDiv.addEventListener("click", click);
});

//remove listener

radio2.addEventListener("change", () => {
  existingDiv.removeEventListener("click", click);
  newDiv.removeEventListener("click", click);
});

//dispatch
//działa asynchronicznie

radio3.addEventListener("click", () => {
  existingDiv.removeEventListener("click", click);
  newDiv.removeEventListener("click", click);

  var newEvent = new Event("myEvent");

  existingDiv.addEventListener("myEvent", click);
  newDiv.addEventListener("myEvent", click);

  existingDiv.dispatchEvent(newEvent);
  newDiv.dispatchEvent(newEvent);
});

//event function

function click(event) {
  let element = event.target;

  let span = document.createElement("span");
  span.textContent = "click!";
  span.className = "inner-span";
  element.appendChild(span);

  setTimeout(() => {
    element.removeChild(span);
  }, 1000);
}

//Tables

//Properties

function createEvent(element, property, DOMel) {
  let el = document.querySelector(element);

  el.textContent = eval(`DOMel${property}`);
  el.previousElementSibling.addEventListener("click", () =>
    console.log(eval(`DOMel${property}`))
  );
}

createEvent(".NodeBaseURI", ".baseURI", newDiv);
createEvent(".NodeChildNodes", ".childNodes", newDiv);
createEvent(".NodeFirstChild", ".firstChild", newDiv);
createEvent(".NodeIsConnected", ".isConnected", newDiv);
createEvent(".NodeLastChild", ".lastChild", newDiv);
createEvent(".NodeNextSibling", ".nextSibling", newDiv);
createEvent(".NodeNodeName", ".nodeName", newDiv);
createEvent(".NodeNodeType", ".nodeType", newDiv);
createEvent(".NodeNodeValue", ".nodeValue", newDiv);
createEvent(".NodeOwnerDocument", ".ownerDocument", newDiv);
createEvent(".NodeParentNode", ".parentNode", newDiv);
createEvent(".NodeParentElement", ".parentElement", newDiv);
createEvent(".NodePreviousSibling", ".previousSibling", newDiv);
createEvent(".NodeTextContent", ".textContent", newDiv);

createEvent(".ElementAttributes", ".attributes", existingDiv);
createEvent(".ElementClassList", ".classList", existingDiv);
createEvent(".ElementClassName", ".className", existingDiv);
createEvent(".ElementClientHeight", ".clientHeight", existingDiv);
createEvent(".ElementClientLeft", ".clientLeft", existingDiv);
createEvent(".ElementID", ".id", existingDiv);
createEvent(".innerHTML", ".innerHTML", existingDiv);
createEvent(".localName", ".localName", existingDiv);
createEvent(".namespaceURI", ".namespaceURI", existingDiv);
createEvent(".outerHTML", ".outerHTML", existingDiv);
createEvent(".prefix", ".prefix", existingDiv);
createEvent(".scrollHeight", ".scrollHeight", existingDiv);
createEvent(".scrollTop", ".scrollTop", existingDiv);
createEvent(".tagName", ".tagName", existingDiv);

//methods

//Node.appendChild()

let futureParent, futureparent2;
futureParent = document.querySelector(".NodeAppendChild");
futureParent.addEventListener(
  "click",
  addChild.bind(this, futureParent, "left-child")
);

//Node.cloneNode()

futureParent = document.querySelector(".cloneNode");
addChild(futureParent, "left-child");
futureParent.addEventListener("click", e => {
  let copy = e.target.nextElementSibling.firstChild.cloneNode(true);
  e.target.nextElementSibling.appendChild(copy);
});

//compareDocumentPosition"()

futureParent = document.querySelector(".compareDocumentPosition");
addChild(futureParent, "left-child");
addChild(futureParent, "right-child");
futureParent.addEventListener("click", e => {
  let first = e.target.nextElementSibling.firstChild;
  let last = e.target.nextElementSibling.lastChild;
  let compare = first.compareDocumentPosition(last);
  let newText = document.createTextNode(compare);
  e.target.nextElementSibling.appendChild(newText);
});

//NodeContains()
futureParent = document.querySelector(".NodeContains");
addChild(futureParent, "right-child");
futureParent.addEventListener("click", e => {
  let parent = e.target.nextElementSibling;
  let child = e.target.nextElementSibling.firstChild;

  let contain = parent.contains(child);
  let newText = document.createTextNode(contain);
  e.target.nextElementSibling.appendChild(newText);
});

//removeChild"()

futureParent = document.querySelector(".removeChild");

for (let i = 0; i < 7; i++) {
  addChild(futureParent, "left-child");
}
futureParent.addEventListener("click", e => {
  let parent = e.target.nextElementSibling;
  let child = e.target.nextElementSibling.lastChild;

  if (parent.firstChild) {
    parent.removeChild(child);
  }
});

//Element.getAttribute()

futureParent = document.querySelector(".get-attribute");

futureParent.addEventListener("click", e => {
  e.target.nextElementSibling.textContent = futureParent.getAttribute("class");
  console.log(futureParent.getAttribute("class"));
});

//getAttributeNames()

futureParent2 = document.querySelector(".getAttributeNames");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.textContent = futureParent2.getAttributeNames();
  console.log(futureParent2.getAttributeNames());
});

//getBoundingClientRect

futureParent2 = document.querySelector(".getBoundingClientRect");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.textContent = futureParent2.getBoundingClientRect();
  console.log(futureParent2.getBoundingClientRect());
});

//getElementsByClassName

futureParent2 = document.querySelector(".get-elements-by-className");
addChild(futureParent2, "right-child");

futureParent2.addEventListener("click", e => {
  let contain = e.target.nextElementSibling.getElementsByClassName(
    "right-child"
  );
  let newText = document.createTextNode(contain);
  e.target.nextElementSibling.appendChild(newText);
  console.log(
    e.target.nextElementSibling.getElementsByClassName("right-child")
  );
});

//Element.hasAttribute()

futureParent2 = document.querySelector(".hasAttribute");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.textContent = futureParent2.hasAttribute("class");
  console.log(futureParent2.hasAttribute("class"));
});

//insertAdjacentElement()

futureParent2 = document.querySelector(".insertAdjacentElement");

futureParent2.addEventListener("click", e => {
  let el = document.createElement("span");
  el.className = "left-child";

  e.target.nextElementSibling.insertAdjacentElement("beforeend", el);

  console.log("utworzono element!");
});

//insertAdjacentHTML()

futureParent2 = document.querySelector(".insertAdjacentHTML");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.insertAdjacentHTML(
    "beforeend",
    '<span class="left-child"></span>'
  );

  console.log("utworzono element!");
});

//insertAdjacentText

futureParent2 = document.querySelector(".insertAdjacentText");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.insertAdjacentText(
    "beforeend",
    "Utworzony text node!"
  );

  console.log("utworzono element!");
});

//matches

futureParent2 = document.querySelector(".matches");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.textContent = e.target.matches(".matches");

  console.log(e.target.matches(".matches"));
});

//setAttribute

futureParent2 = document.querySelector(".setAttribute");
addChild(futureParent2, "right-child");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.firstChild.setAttribute("class", "blue-child");

  console.log("dodano klasę!");
});
//removeAttribute

futureParent2 = document.querySelector(".removeAttribute");
addChild(futureParent2, "blue-child");

futureParent2.addEventListener("click", e => {
  e.target.nextElementSibling.firstChild.id = "blue-element";
  e.target.nextElementSibling.firstChild.removeAttribute("class");

  console.log("usunięto klasę!");
});

//Funkcje pomocnicze

function addChild(element, className) {
  let el = document.createElement("span");
  el.className = className;
  element.nextElementSibling.appendChild(el);
}
